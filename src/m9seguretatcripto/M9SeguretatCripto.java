package m9seguretatcripto;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.RandomAccessFile;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.SecureRandom;
import java.security.Signature;
import java.security.SignatureException;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.Scanner;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.CipherInputStream;
import javax.crypto.CipherOutputStream;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

/**
 *
 * @author David i Ángel - 2n DAM
 */
public class M9SeguretatCripto extends Thread {

    static Scanner teclado = new Scanner(System.in);

    /**
     * Mostra el menú i et mou per les diferents opcions del programa
     * @param args
     * @throws java.io.IOException -> Tractament de fitxers
     * @throws java.lang.ClassNotFoundException -> Per si no troba la classe
     * @throws java.security.NoSuchAlgorithmException -> Per si no troba l'algoritme
     * @throws java.io.FileNotFoundException -> Per si no troba un arxiu
     * @throws java.security.InvalidKeyException -> Per si la clau no té la mida de bytes adeqüada
     * @throws javax.crypto.NoSuchPaddingException -> Per si no es compleix la instància del Chiper
     * @throws java.security.spec.InvalidKeySpecException -> Per fer el init a la clau
     * @throws java.security.SignatureException -> Per signar la clau
     * @throws javax.crypto.IllegalBlockSizeException -> Per fer el wrap del Chiper (amb clau)
     * @throws javax.crypto.BadPaddingException -> Per fer el .doFinal a partir del Chiper
     */
    public static void main(String[] args) throws IOException, NoSuchAlgorithmException, ClassNotFoundException, FileNotFoundException, NoSuchPaddingException, InvalidKeyException, InvalidKeySpecException, IllegalBlockSizeException, BadPaddingException, SignatureException {

        int opcions = 0;
        while (opcions != 5) {
            menúPrincipal();
            System.out.println("Selecciona una opció:");
            opcions = teclado.nextInt();
            switch (opcions) {
                case 1: //1) Operacions amb Hash
                    int res_menu = 0;
                    while (res_menu != 4) {
                        menúHash();
                        System.out.println("Selecciona una opció:");
                        res_menu = teclado.nextInt();
                        switch (res_menu) {
                            case 1:
                                generarHash();
                                System.out.println("");
                                break;
                            case 2:
                                comprovarHashI();
                                System.out.println("");
                                break;
                            case 3:
                                comprovarHashII();
                                System.out.println("");
                                break;
                            default:
                                break;
                        }
                    }
                    break;
                case 2: //2) Xifrat simètric
                    res_menu = 0;
                    while (res_menu != 3) {
                        menúXifratSimètric();
                        System.out.println("Selecciona una opció:");
                        res_menu = teclado.nextInt();
                        switch (res_menu) {
                            case 1:
                                xifrarFitxer();
                                System.out.println("");
                                break;
                            case 2:
                                desxifrarFitxer();
                                System.out.println("");
                                break;
                            default:
                                break;
                        }
                    }
                    break;
                case 3: //3) Signatura digital
                    signaturaDigital();
                    System.out.println("");
                    break;
                case 4: //4) Criptografia asimètrica
                    criptografiaAsimètrica();
                    System.out.println("");
                    break;
                default:
                    break;
            }
        }
    }

    private static void menúPrincipal() {
        System.out.println("Introdueix l'opció:");
        System.out.println("-------------------");
        System.out.println("1) Operacions amb Hash\n2) Xifrat simètric\n3) Signatura digital\n4) Criptografia asimètrica\n5) Sortir\n");
    }

    private static void menúHash() {
        System.out.println("Introdueix l'opció:");
        System.out.println("-------------------");
        System.out.println("1) Generació i emmagatzematge de Hash\n2) Comprovació de Hash: Introducció per teclat\n3) Comprovació de Hash II: Utilitzant fitxer\n4) Sortir");
    }

    private static void menúXifratSimètric() {
        System.out.println("Introdueix l'opció:");
        System.out.println("-------------------");
        System.out.println("1) Xifrar un fitxer\n2) Desxifrar un fitxer\n3) Sortir");
    }

    /**
     * MÈTODE PER GENERAR EL HASH. A partir de dos fitxers creats per l'usuari es genera
     * un fitxer amb el missatge introduit per l'usuari, que ho introdueix en text pla,  
     * i un altre on s'emmagatzema el hash del missatge anterior. Es mostra el contingut
     * del missatge a partir del fitxer "pla" i després es mostra el SHA del Hash.
     * @throws java.io.IOException -> Tractament de fitxers
     * @throws java.lang.ClassNotFoundException -> Per si no troba la classe
     * @throws java.security.NoSuchAlgorithmException -> Per si no troba l'algoritme
     */
    private static void generarHash() throws IOException, ClassNotFoundException, NoSuchAlgorithmException {
        teclado.nextLine();
        System.out.println("Introdueix el nom del fitxer que emmagatzemarà el missatge. Ex: 'missatge.dat':");
        String fitxer = teclado.nextLine();

        File crearFitxerMissatge = new File("hash/", fitxer);
        crearFitxerMissatge.createNewFile();

        System.out.println("Introdueix el nom del fitxer que emmagatzemarà el Hash del missatge. Ex: 'missatgeHash.dat':");
        String fitxerHash = teclado.nextLine();

        File crearFitxerHash = new File("hash/", fitxerHash);
        crearFitxerHash.createNewFile();

        System.out.println("Introdueix el missatge. Per exemple: 'Hola'");
        String missatge = teclado.nextLine();

        RandomAccessFile raf = new RandomAccessFile(crearFitxerMissatge, "rw");
        raf.setLength(0);
        raf.write(missatge.getBytes());

        raf.seek(0);

        String dades = raf.readLine();

        raf.close();
        System.out.println("El teu missatge és: " + dades);

        MessageDigest md = MessageDigest.getInstance("SHA-512");
        md.update(missatge.getBytes());
        byte digest[] = md.digest();
        FileOutputStream fos = new FileOutputStream("hash/" + fitxerHash);
        ObjectOutputStream oos = new ObjectOutputStream(fos);
        oos.writeObject(missatge);

        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < digest.length; i++) {
            sb.append(String.format("%02x", digest[i]));
        }
        String hash = sb.toString();

        System.out.println("El HASH (SHA-512): " + hash.toUpperCase());
    }

    /**
     * MÈTODE PER COMPROVAR EL HASH PER TECLAT. Es demana a l'usuari introduir el hash
     * de forma hexadecimal i el fitxer que conté el missatge en text pla. Es passa el 
     * contingut del fitxer a hash i s'iguala al que ha introduit l'usuari per teclat.
     * Si coincideixen:
     *  - Es mostra un missatge anunciant que no s'ha alterat l'arxiu i el seu contingut
     * Si no coincideixen:
     *  - Es mostra un missatge dient que s'ha alterat el contingut
     * @throws java.io.IOException -> Tractament de fitxers
     * @throws java.io.FileNotFoundException -> Per si no troba un arxiu
     * @throws java.security.NoSuchAlgorithmException -> Per si no troba l'algoritme
     */
    private static void comprovarHashI() throws NoSuchAlgorithmException, FileNotFoundException, IOException {
        System.out.println("Introdueix el HASH (format Hexadecimal).\nPer exemple 'C096860BE238D7E0A6D3929C7BA06F468D3E6B7B28132BA48D553F845788C513004B1EC78758F24E9E1F006AE9A89651F80023F5505927A7AECD6529FA12C081': ");
        String hash = teclado.next();

        System.out.println("Introdueix el nom del fitxer que emmagatzema el missatge. Per exemple 'missatge.dat': ");
        String fitxer = teclado.next();

        File fichero = new File("hash/" + fitxer);

        if (fichero.exists()) {
            FileReader fr = new FileReader("hash/" + fitxer);
            BufferedReader br = new BufferedReader(fr);
            String cadena = br.readLine();

            MessageDigest md = MessageDigest.getInstance("SHA-512");
            md.update(cadena.getBytes());
            byte digest[] = md.digest();

            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < digest.length; i++) {
                sb.append(String.format("%02x", digest[i]));
            }
            String result = sb.toString();

            if (result.toUpperCase().equals(hash.toUpperCase())) {
                System.out.println("El missatge no ha estat alterat.\nEl seu contingut és: " + cadena);
            } else {
                System.out.println("El missatge ha estat alterat.");
            }
        } else {
            System.out.println("El fitxer amb nom '" + fitxer + "' no existeix.");
        }
    }

    /**
     * MÈTODE PER COMPROVAR EL HASH PER FITXERS. L'usuari indica quin fitxer conté el
     * missatge pla i un altre amb el Hash. A partir del pla, es genera un hash. Segui-
     * dament es llegeix el contingut del fitxer hash i es compara amb el hash obtingut
     * del fitxer pla.
     * Si coincideixen:
     *  - Es mostra un missatge anunciant que no s'ha alterat l'arxiu i el seu contingut
     * Si no coincideixen:
     *  - Es mostra un missatge dient que s'ha alterat el contingut
     * @throws java.io.IOException -> Tractament de fitxers
     * @throws java.lang.ClassNotFoundException -> Per si no troba la classe
     * @throws java.security.NoSuchAlgorithmException -> Per si no troba l'algoritme
     * @throws java.io.FileNotFoundException -> Per si no troba un arxiu
     */
    private static void comprovarHashII() throws FileNotFoundException, IOException, NoSuchAlgorithmException, ClassNotFoundException {
        System.out.println("Introdueix el nom del fitxer que emmagatzema el missatge. Per exemple 'missatge.dat': ");
        String nomFitxer = teclado.next();

        System.out.println("Introdueix el nom del fitxer que emmagatzema el Hash del missatge. Per exemple 'missatgeHash.dat': ");
        String nomHash = teclado.next();

        File fichero = new File("hash/" + nomFitxer);
        File hash = new File("hash/" + nomHash);

        if (fichero.exists() && hash.exists()) {
            FileReader fr = new FileReader("hash/" + nomFitxer);
            BufferedReader br = new BufferedReader(fr);
            String cadena = br.readLine(); //Mensaje en texto plano

            MessageDigest md = MessageDigest.getInstance("SHA-512");
            md.update(cadena.getBytes());
            byte digest[] = md.digest();

            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < digest.length; i++) {
                sb.append(String.format("%02x", digest[i]));
            }
            String sha = sb.toString();

            FileInputStream fileIn = new FileInputStream("hash/" + nomHash);
            ObjectInputStream ois = new ObjectInputStream(fileIn);
            // Obtenim la cadena
            Object dadesMissatge = ois.readObject();
            String dades = (String) dadesMissatge;

            md = MessageDigest.getInstance("SHA-512");
            md.update(dades.getBytes());
            byte digest2[] = md.digest();

            StringBuilder sb2 = new StringBuilder();
            for (int i = 0; i < digest2.length; i++) {
                sb2.append(String.format("%02x", digest2[i]));
            }
            String hash2 = sb2.toString();

            //System.out.println("sha: " + sha.toUpperCase() + "\nhash2: " + hash2.toUpperCase());
            if (sha.toUpperCase().equals(hash2.toUpperCase())) {
                System.out.println("El missatge no ha estat alterat.\nEl seu contingut és: " + cadena);
            } else {
                System.out.println("El missatge ha estat alterat.");
            }
        } else if (!fichero.exists()) {
            System.out.println("El fitxer amb nom '" + nomFitxer + "' no existeix.");
        } else {
            System.out.println("El fitxer amb nom '" + nomHash + "' no existeix.");
        }
    }

    /**
     * MÈTODE PER XIFRAR UN FITXER. Es demana a l'usuari introduir el nom del fitxer que es
     * vol xifrar, i a continuació el password a emprar. Es genera la clau amb el tamany
     * de bytes de la contrasenya introduida. Seguidament es genera la sortida del Chiper
     * a partir de la lectura del fitxer que ha seleccionat l'usuari per xifrar.
     * Si s'ha xifrat correctament:
     *  - Es mostra un missatge anunciant que l'arxiu s'ha xifrat amb la clau secreta.
     * Si no s'ha xifrat:
     *  - Es demana una mida que sigui de 16, 24 o 32 bytes (AES)
     * @see https://ca.wikipedia.org/wiki/Advanced_Encryption_Standard
     * @throws java.io.IOException -> Tractament de fitxers
     * @throws java.security.NoSuchAlgorithmException -> Per si no troba l'algoritme
     * @throws java.io.FileNotFoundException -> Per si no troba un arxiu
     * @throws java.security.InvalidKeyException -> Per si la clau no té la mida de bytes adeqüada
     * @throws javax.crypto.NoSuchPaddingException -> Per si no es compleix la instància del Chiper
     */
    private static void xifrarFitxer() throws FileNotFoundException, IOException, InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException {
        System.out.println("Introdueix el nom del fitxer que vols xifrar. Per exemple 'enigma.jpg' :");
        String nomFitxer = teclado.next();

        System.out.println("Introdueix el password que vols utilitzar per a xifrar. Per exemple 'Enigma ist nicht zu entziffern': ");
        String pass = teclado.next();

        File fitxer = new File("crypt/" + nomFitxer);
        fitxer.createNewFile();

        SecretKeySpec clau = new SecretKeySpec(pass.getBytes(), "AES");
        Cipher c = Cipher.getInstance("AES/ECB/PKCS5Padding");

        try {
            c.init(Cipher.ENCRYPT_MODE, clau);

            File fitxerXifrat = new File("crypt/" + nomFitxer + ".aes");
            fitxerXifrat.createNewFile();

            FileInputStream filein = new FileInputStream(fitxer);

            CipherOutputStream out = new CipherOutputStream(new FileOutputStream(fitxerXifrat), c);
            int midaBloc = c.getBlockSize();
            byte[] bytes = new byte[midaBloc];

            int i = filein.read(bytes);
            while (i != -1) {
                out.write(bytes, 0, i);
                i = filein.read(bytes);
            }
            out.flush();
            out.close();
            filein.close();
            System.out.println("Arxiu xifrat amb clau secreta.");
        } catch (Exception e) {
            System.out.println("La mida de la clau ha de ser de 16, 24 o 32 bytes");
        }
    }

    /**
     * MÈTODE PER DESXIFRAR UN FITXER. Es demana a l'usuari introduir el nom del fitxer que es
     * vol desxifrar, i a continuació el password a emprar. Es comprova que el fitxer existeixi,
     * si no es així, es mostra un missatge dient que el fitxer no existeix. Es genera la clau 
     * amb el tamany de bytes de la contrasenya introduida. A partir d'aquí es genera la entrada 
     * del Chiper a partir de la lectura del fitxer que ha seleccionat l'usuari per desxifrar.
     * Es generarà un nou fitxer amb extensió .clar que es el fitxer introduit desxifrat
     * Si s'ha desxifrat correctament:
     *  - Es mostra un missatge anunciant que l'arxiu s'ha dexifrat amb la clau secreta.
     * Si no s'ha desxifrat:
     *  - Es demana una mida que sigui de 16, 24 o 32 bytes (AES)
     * @see https://ca.wikipedia.org/wiki/Advanced_Encryption_Standard
     * @throws java.io.IOException -> Tractament de fitxers
     * @throws java.security.NoSuchAlgorithmException -> Per si no troba l'algoritme
     * @throws java.security.InvalidKeyException -> Per si la clau no té la mida de bytes adeqüada
     * @throws javax.crypto.NoSuchPaddingException -> Per si no es compleix la instància del Chiper
     */
    private static void desxifrarFitxer() throws IOException, NoSuchAlgorithmException, InvalidKeyException, NoSuchPaddingException {
        System.out.println("Introdueix el nom del fitxer que vols desxifrar. Per exemple 'enigma.jpg.aes' :");
        String nomFitxer = teclado.next();

        System.out.println("Introdueix el password que vols utilitzar per a desxifrar. Per exemple 'Enigma ist nicht zu entziffern': ");
        String pass = teclado.next();

        File fitxer = new File("crypt/" + nomFitxer);

        if (fitxer.exists()) {

            SecretKeySpec clau = new SecretKeySpec(pass.getBytes(), "AES");
            Cipher c = Cipher.getInstance("AES/ECB/PKCS5Padding");

            try {
                c.init(Cipher.DECRYPT_MODE, clau);

                CipherInputStream in = new CipherInputStream(new FileInputStream(fitxer), c);

                File fitxerDesxifrat = new File("crypt/" + nomFitxer + ".clar");
                fitxerDesxifrat.createNewFile();

                int tambloque = c.getBlockSize();
                byte[] bytes = new byte[tambloque];
                tambloque = c.getBlockSize();
                bytes = new byte[tambloque];

                FileOutputStream fileout = new FileOutputStream(fitxerDesxifrat);

                int i = in.read(bytes);
                while (i != -1) {
                    fileout.write(bytes, 0, i);
                    i = in.read(bytes);
                }
                fileout.close();
                in.close();
                System.out.println("Arxiu desxifrat amb clau secreta.");
            } catch (Exception e) {
                System.out.println("La mida de la clau ha de ser de 16, 24 o 32 bytes");
            }
        } else {
            System.out.println("El fitxer amb nom '" + nomFitxer + "' no existeix.");
        }
    }

    /**
     * MÈTODE PER SIGNAR DIGITALMENT. Es demana a l'usuari introduir el nom del fitxer que es
     * vol signar. A continuació es generen el parell de claus, una privada i una pública. La
     * privada s'utilitza per signar i la pública per verificar. Es desen les claus en dos
     * fitxers diferents, i partir de la privada es signa digitalment el arxiu anteriorment
     * creat per l'usuari. Finalment s'emmagatzema la signatura en el arxiu: missatgeSecret.signatura
     * i s'avisa a l'usuari mostrant un avís de que el procés ha sigut estat amb èxit
     * @see https://www.tutorialspoint.com/java_cryptography/java_cryptography_creating_signature.htm
     * @throws java.io.IOException -> Tractament de fitxers
     * @throws java.security.NoSuchAlgorithmException -> Per si no troba l'algoritme
     * @throws java.security.InvalidKeyException -> Per si la clau no té la mida de bytes adeqüada
     * @throws java.security.spec.InvalidKeySpecException -> Per fer init a la clau
     * @throws java.security.SignatureException -> Per signar
     */
    private static void signaturaDigital() throws NoSuchAlgorithmException, IOException, InvalidKeySpecException, InvalidKeyException, SignatureException{
        System.out.println("Introdueix el nom del fitxer a signar. Per exemple 'missatgeSecret.dat'");
        String nomFitxer = teclado.next();

        File fitxer = new File("signatures/" + nomFitxer);
        fitxer.createNewFile();

        KeyPairGenerator keyGen = KeyPairGenerator.getInstance("DSA");
        SecureRandom numero = SecureRandom.getInstance("SHA1PRNG");
        keyGen.initialize(1024, numero);
        KeyPair par = keyGen.generateKeyPair();
        PrivateKey claupriv = par.getPrivate();
        PublicKey claupub = par.getPublic();

        PKCS8EncodedKeySpec pk8Spec = new PKCS8EncodedKeySpec(claupriv.getEncoded());
        FileOutputStream outpriv = new FileOutputStream("signatures/Clau.privada");
        outpriv.write(pk8Spec.getEncoded());
        outpriv.close();

        X509EncodedKeySpec pkX509 = new X509EncodedKeySpec(claupub.getEncoded());
        FileOutputStream outpub = new FileOutputStream("signatures/Clau.publica");
        outpub.write(pkX509.getEncoded());
        outpub.close();

        FileInputStream inpriv = new FileInputStream("signatures/Clau.privada");
        byte[] bufferPriv = new byte[inpriv.available()];
        inpriv.read(bufferPriv);
        inpriv.close();

        PKCS8EncodedKeySpec clauPrivadaSpec = new PKCS8EncodedKeySpec(bufferPriv);
        KeyFactory keyDSA = KeyFactory.getInstance("DSA");
        PrivateKey clauPrivada = keyDSA.generatePrivate(clauPrivadaSpec);

        Signature dsa = Signature.getInstance("SHA1withDSA");
        dsa.initSign(clauPrivada);

        FileInputStream arxiu = new FileInputStream("signatures/" + nomFitxer);

        BufferedInputStream bis = new BufferedInputStream(arxiu);
        byte[] buffer = new byte[1024];
        int len;
        while ((len = bis.read(buffer)) >= 0) {
            dsa.update(buffer, 0, len);
        }
        bis.close();
        byte[] signatura = dsa.sign();
        
        FileOutputStream fos = new FileOutputStream("signatures/missatgeSecret.signatura");
        fos.write(signatura);
        fos.close();

        System.out.println("Signatura digital generada.");
    }

    /**
     * MÈTODE PER ENCRIPTAR ASIMÈTRICAMENT. Per començar es demana a l'usuari el missatge que
     * es vol encriptar. A continuació, es generen la clau privada i la pública i seguidament,
     * s'encripta amb el mòde wrap del Chiper la instància al RSA. Després es xifra el missatge
     * amb la clau secreta i es mostra per pantalla. Per "tornar a tenir" el missatge sense
     * encriptar s'ha d'instànciar al mètode UNWRAP a partir de la clau privada.
     * @throws java.security.NoSuchAlgorithmException -> Per si no troba l'algoritme
     * @throws java.security.InvalidKeyException -> Per si la clau no té la mida de bytes adeqüada
     * @throws javax.crypto.NoSuchPaddingException -> Per si no es compleix la instància del Chiper
     * @throws java.security.spec.InvalidKeySpecException -> Per fer el init a la clau
     * @throws javax.crypto.IllegalBlockSizeException -> Per fer el wrap del Chiper (amb clau)
     * @throws javax.crypto.BadPaddingException -> Per fer el .doFinal a partir del Chiper
     */
    private static void criptografiaAsimètrica() throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException {
        System.out.println("Introdueix el missatge que vols encriptar:");
        String missatge = teclado.next();
        
        KeyPairGenerator keyGen = KeyPairGenerator.getInstance("RSA");
        keyGen.initialize(1024);
        KeyPair par = keyGen.generateKeyPair();
        PrivateKey claupriv = par.getPrivate();
        PublicKey claupub = par.getPublic();

        // Es crea la clau SECRETA AES
        KeyGenerator kg = KeyGenerator.getInstance("AES");
        kg.init(128);
        SecretKey clauSecreta = kg.generateKey();

        // S'encripta(wrap) la clau secreta amb la clau RSA pública
        Cipher c = Cipher.getInstance("RSA/ECB/PKCS1Padding");
        c.init(Cipher.WRAP_MODE, claupub);
        byte clauEmbolcallada[] = c.wrap(clauSecreta);

        // Xifrem el text amb la clau secreta
        c = Cipher.getInstance("AES/ECB/PKCS5Padding");
        c.init(Cipher.ENCRYPT_MODE, clauSecreta);
        byte textPla[] = missatge.getBytes();
        byte textXifrat[] = c.doFinal(textPla);
        System.out.println("Missatge encriptat: " + new String(textXifrat));

        // Es desencripta la clau secreta amb la clau RSA privada
        Cipher c2 = Cipher.getInstance("RSA/ECB/PKCS1Padding");
        c2.init(Cipher.UNWRAP_MODE, claupriv);
        Key clauDesembolcallada = c2.unwrap(clauEmbolcallada, "AES", Cipher.SECRET_KEY);

        // Desxifrem amb la clau desembolcallada 
        c2 = Cipher.getInstance("AES/ECB/PKCS5Padding");
        c2.init(Cipher.DECRYPT_MODE, clauDesembolcallada);
        byte desencriptat[] = c2.doFinal(textXifrat);
        System.out.println("Missatge desencriptat: " + new String(desencriptat));
    }
}
