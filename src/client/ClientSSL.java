package client;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.Scanner;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;

/**
 *
 * @author Ángel i David
 */
public class ClientSSL {

    static Scanner teclado = new Scanner(System.in);
    static int opcions = 0;

    public static void main(String[] args) throws IOException {
        // Paràmetres de connexió
        String Host = "localhost";
        int port = 5000;

        System.setProperty("javax.net.ssl.trustStore", "/home/alumne/NetBeansProjects/M09/UF1/m9_seguretat_criptografia/keyClient/UserSSLKeyStore");
        System.setProperty("javax.net.ssl.trustStorePassword", "654321");
        //angel
        //System.setProperty("javax.net.ssl.trustStore", "C:\\NetBeans Projects\\2DAM\\Curso 2018-2019\\M09\\UF1\\m9_seguretat_criptografia\\keyClient/UserSSLKeyStore");
        //System.setProperty("javax.net.ssl.trustStorePassword", "654321");
  
        // Obtenir el SocketSSL utilitzant el patró factory
        SSLSocketFactory sfact = (SSLSocketFactory) SSLSocketFactory.getDefault();
        SSLSocket client = (SSLSocket) sfact.createSocket(Host, port);
        
        menu(client);
        
        // Establir el flux de sortida cap al servidor
        DataOutputStream fluxSortida = new DataOutputStream(client.getOutputStream());
        //Salutació al servidor
        fluxSortida.writeUTF("Salutacions al servidor del client...");
        //flux d'entrada del servidor
        DataInputStream fluxEntrada = new DataInputStream(client.getInputStream());
        //Rebem salutació del servidor
        System.out.println("Rebent del servidor: \n\t" + fluxEntrada.readUTF());

        // Tanquem recursos
        fluxEntrada.close();
        fluxSortida.close();
        client.close();
    }

    private static void menu(SSLSocket client) throws IOException {
        DataInputStream entrada = new DataInputStream(client.getInputStream());
        DataOutputStream sortida = new DataOutputStream(client.getOutputStream());
        
        while (opcions != 3) {
            System.out.println("Introdueix l'opció:");
            System.out.println("-------------------");
            System.out.println("1) Operacions amb Hash\n2) Xifrat simètric\n3) Signatura digital\n4) Criptografia asimètrica\n5) Sortir\n");
            System.out.println("Selecciona una opció:");
            opcions = teclado.nextInt();
            
            sortida.write(opcions);
            
            switch (opcions) {
                case 1:
                    int res_menu = 0;
                    while (res_menu != 4) {
                        menuHash();
                        System.out.println("Selecciona una opció:");
                        res_menu = teclado.nextInt();
                        
                        sortida.write(res_menu);
                        
                        switch (res_menu) {
                            case 1:
                                //generarHash();
                                System.out.println("generarHash");
                                break;
                            case 2:
                                //comprovarHashI();
                                System.out.println("comprovarHashI");
                                break;
                            case 3:
                                //comprovarHashII();
                                System.out.println("comprovarHashII");
                                break;
                            default:
                                break;
                        }
                    }
                    break;
                case 2:
                    res_menu = 0;
                    while (res_menu != 3) {
                        menuXifratSimètric();
                        System.out.println("Selecciona una opció:");
                        res_menu = teclado.nextInt();
                        
                        sortida.write(res_menu);
                        
                        switch (res_menu) {
                            case 1:
                                //xifrarFitxer();
                                System.out.println("xifrarFitxer");
                                break;
                            case 2:
                                //desxifrarFitxer();
                                System.out.println("desxifrarFitxer");
                                break;
                            default:
                                break;
                        }
                    }
                case 3:
                    break;
                case 4:
                    break;
                case 5:
                    break;
                default:
                    break;
            }
        }
    }

    private static void menuHash() {
        System.out.println("\nIntrodueix l'opció:");
        System.out.println("-------------------");
        System.out.println("1) Generació i emmagatzematge de Hash\n2) Comprovació de Hash: Introducció per teclat\n3) Comprovació de Hash II: Utilitzant fitxer\n4) Sortir\n");
    }

    private static void menuXifratSimètric() {
        System.out.println("\nIntrodueix l'opció:");
        System.out.println("-------------------");
        System.out.println("1) Xifrar un fitxer\n2) Desxifrar un fitxer\n3) Sortir\n");
    }
}
